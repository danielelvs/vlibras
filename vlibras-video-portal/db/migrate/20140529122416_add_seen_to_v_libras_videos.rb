# -*- encoding : utf-8 -*-
class AddSeenToVLibrasVideos < ActiveRecord::Migration
  def change
    add_column :v_libras_videos, :seen, :boolean
  end
end
