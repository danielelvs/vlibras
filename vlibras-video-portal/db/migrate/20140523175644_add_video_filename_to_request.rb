# -*- encoding : utf-8 -*-
class AddVideoFilenameToRequest < ActiveRecord::Migration
  def change
    add_column :v_libras_requests, :video_filename, :string
  end
end
