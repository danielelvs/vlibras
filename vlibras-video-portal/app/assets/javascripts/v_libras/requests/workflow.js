function debugTime(time) {
    if (window.location.href.indexOf("debug") > -1) {
        return 0.3;
    } else {
        return time;
    };
}

var VLibrasLocalization = function() {
    var _id, _wrapper, _video_wrapper, _video, _options;

    function _initParams() {
        _wrapper = $(".wrapper").filter(_id),
        _video_wrapper = _wrapper.find(".video_wrapper"),
        _video = _wrapper.find("video");
    }

    function _wrapperConfig() {
        _wrapper.width(_options.video.width);
        _wrapper.height(_options.video.height);
    }

    function _videoWrapperConfig() {
        _video_wrapper.css('position', 'absolute');
        _video_wrapper.css('z-index', 1);
        _video_wrapper.width(_options.video.width);
        _video_wrapper.height(_options.video.height);
    }

    function _videoConfig() {
        _video.css('position', 'relative');
        _video.css('z-index', 0);
        _video.attr('width', _options.video.width);
        _video.attr('height', _options.video.height);
    }

    function _addButtons() {

        var time_helper = null;

        var interval = setInterval(function() {
            var time = Math.round( _video[0].currentTime * 10 ) / 10;

            if (time !== time_helper) {
                for (var i = 0; i < _options.buttons.length; i++)
                    _createButton(i, time);

                time_helper = time;
            }
        }, 200);
    }

    function _createButton(index, time) {
        if ((time >= _options.buttons[index].start) && (_options.buttons[index].shown != true)) {
            _options.buttons[index].shown = true;
            console.debug("[VLibrasLocalization] showing " + _options.buttons[index]);

            var image = null;

            if (_options.buttons[index].clickable === true) {
                image = $('<a href="#"><img src="' + _options.buttons[index].path + '"></a>');
            } else {
                image = $('<img src="' + _options.buttons[index].path + '">');
            }

            image.attr('id', _options.buttons[index].id);
            image.data('value', _options.buttons[index].value);
            image.attr('data-value', _options.buttons[index].value);
            image.css('position', 'absolute');
            image.css('top', _options.buttons[index].y);
            image.css('left', _options.buttons[index].x);

            image.hide().appendTo(_video_wrapper).fadeIn(_options.buttons[index].delay);
        }

        if ((time >= _options.buttons[index].end) && (_options.buttons[index].hidden != true) && (_options.buttons[index].shown === true)) {
            console.debug("[VLibrasLocalization] hiding " + _options.buttons[index]);
            _options.buttons[index].hidden = true;

            $('#' + _options.buttons[index].id).fadeOut(_options.buttons[index].delay);
        }
    }

    return {
        init: function(id, optns) {
            _id = id;
            _options = optns;

            _initParams();
            _wrapperConfig();
            _videoWrapperConfig();
            _videoConfig();
            _addButtons();
        },

        play: function() {
            _video[0].play();
        },

        stop: function() {
            _video[0].pause();
        }
    }
};