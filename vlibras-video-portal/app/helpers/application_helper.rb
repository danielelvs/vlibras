# -*- encoding : utf-8 -*-
module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def bootstrap_class_for(flash_type)
    case flash_type.to_s
      when "success"
        "alert-success"   # Green
      when "error"
        "alert-danger"    # Red
      when "alert"
        "alert-warning"   # Yellow
      when "notice"
        "alert-info"      # Blue
      else
        flash_type
    end
  end

  def request_status_label(request)
    classes = [ 'badge' ]

    status = request.status

    case status
      when 'created'
      when 'processing'
        classes << 'badge-warning'
      when 'error'
        classes << 'badge-important'
      when 'success'
        classes << 'badge-success'
    end

    content_tag(:a, t(status, scope: 'status'), :class => classes,
                :data => { 'toggle' => "tooltip" }, :title => request.response)
  end

  def html5_video_tag(url, id, classes, data_options = nil, *args)
    options = args.first || {}

    options[:id] = id
    options[:class] = "video-js vjs-default-skin vjs-big-play-centered #{classes}"
    options[:width] = 'auto'
    options[:height] = 'auto'
    options['data-setup'] = data_options if data_options != nil

    content_tag(:video, options) do
      if url.class == String
        content_tag(:source, '', :src => (url + "?t=" + Time.now.getutc.to_i.to_s), :type => mimetype_from_url(url))
      else
        url.each do |v|
          concat content_tag(:source, '', :src => (v + "?t=" + Time.now.getutc.to_i.to_s), :type => mimetype_from_url(v))
        end
      end
    end
  end

  def mimetype_from_url(url)
    return 'video/mp4' if url.split('.').last == 'mp4'
    return 'video/webm' if url.split('.').last == 'webm'

    'video/mp4'
  end

  def include_videojs_assets
    content_for(:css) do
      stylesheet_link_tag "http://vjs.zencdn.net/4.6/video-js.css"
    end

    content_for(:js) do
      javascript_include_tag "http://vjs.zencdn.net/4.6/video.js"
    end
  end

end
