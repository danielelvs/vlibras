# -*- encoding : utf-8 -*-
ActiveAdmin.register VLibras::Video do
  menu :priority => 15

  permit_params :url, :request_id, :seen
end
