# -*- encoding : utf-8 -*-
class VLibras::VideosController < InheritedResources::Base
  actions :show, :destroy

  before_filter :check_vlibras_api_status, :only => [ :index, :show ]
  before_filter :authenticate_user!

  before_filter :mark_as_seen!, :only => [ :show ]

  def index
    @videos = current_user.videos.page(params[:page]).per(9)
    @requests = current_user.requests.limit(6)
  end

protected
  def mark_as_seen!
    @video = VLibras::Video.find(params[:id])
    @video.mark_as_seen!
  end
end
