# -*- encoding : utf-8 -*-
class VLibras::RequestsController < ApplicationController
  protect_from_forgery with: :null_session, :only => [ :callback ]

  before_filter :authenticate_user!, :except => [ :callback ]
  before_filter :check_vlibras_api_status, :except => [ :callback ]

  def rapid
    @request = VLibras::Request.new
  end

  def new

  end

  def create
    @request = VLibras::Request.build_from_params(params, current_user)

    if @request.save
      @request.perform_request(@request.files)
      p 'rerouitsme1'

      flash[:success] = 'Sua requisição foi submetida com sucesso!'

      render json: { status: 'ok', redirect_to: v_libras_videos_path('video-wait' => true)}, status: 200
    else
      flash[:error] = 'Algo deu errado com a sua requisição. Por favor verifique opções escolhidas.'
      flash[:warning] = @request.errors.full_messages.to_sentence.humanize

      # Warning: this code is also present on #perform_request, if the request is successfully done
      @request.files.values.each { |f| f.file.delete }

      # same as redirect :back
      render json: { status: 'error', redirect_to: request.headers["Referer"]}, status: 400
    end
  end

  def callback
    ApiClient::CallbackProcessor.process(params)

    render :text => "OK!"
  end
end
