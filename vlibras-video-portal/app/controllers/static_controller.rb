# -*- encoding : utf-8 -*-
class StaticController < ApplicationController
  before_filter :authenticate_user!

  def home
	redirect_to new_v_libras_request_path
  end
end
