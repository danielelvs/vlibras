(function(validador, $, undefined) {

	var baseUrl = '';
	var serverBackendUrl = '';
	var apiDBHostUrl = '';
	var pybossaEndpoint = '';
	var projectName = '';
	var currentTaskId = -1;

	function _getLoggedUser() {
		var pybossaRememberToken = Cookies.get('remember_token');
		var splittedTokenId = pybossaRememberToken.split('|');
		return splittedTokenId.length > 0 ? splittedTokenId[0] : 'anonymous';
	}

	function _disableFinishButton() {
		$('#finish-button').off('click');
		$('#finish-button').removeClass('enabled-button');
		$('#finish-button').addClass('disabled-button');
	}

	function _enableFinishButton(task, deferred) {
		$('#finish-button').removeClass('disabled-button');
		$('#finish-button').addClass('enabled-button');
		$('#finish-button').off('click').on('click', function() {
			_readAnswer(task, deferred, false);
		});
	}

	function _hideCommentAvatar() {
		$('#comment-avatar').hide();
		$('#finish-task-container').removeClass('finish-padding-top-adjust');
	}

	function _hideCommentRef() {
		$('#comment-ref').hide();
	}

	function resetComments() {
		$('#comment-avatar textarea').val('')
		$('#comment-ref textarea').val('');
		_hideCommentAvatar();
		_hideCommentRef();
	}

	function _enableLoading() {
		$('#loading-container').show();
		$('#main-container').addClass('mask');
	}

	function _disableLoading() {
		$('#loading-container').hide();
		$('#main-container').removeClass('mask');
	}

	function _handleAvatarRadioClick(el, task, deferred) {
		var isIncorrectButton = $(el).val() == 'INCORRECT';
		if (isIncorrectButton) {
			_hideCommentRef();
			$('#comment-avatar').show();
			$('#finish-task-container').addClass('finish-padding-top-adjust');
		} else {
			_hideCommentAvatar();
		}
		var isRefChecked = $('#ref-radio-answers input[type=radio]').is(
				':checked');
		if (isRefChecked) {
			_enableFinishButton(task, deferred);
		} else {
			_disableFinishButton();
		}
	}

	function _handleRefRadioClick(el, task, deferred) {
		var isIncorrectButton = $(el).val() == 'INCORRECT';
		if (isIncorrectButton) {
			$('#incorrect-avatar-button').prop('checked', true);
			$('#comment-ref').show();
			_hideCommentAvatar();
		} else {
			var isAvatarDisabled = $('#avatar-radio-answers input[type=radio]')
					.is(':disabled');
			if (isAvatarDisabled) {
				$('#incorrect-avatar-button').prop('checked', false);
			}
			_hideCommentRef();
		}
		$('#avatar-radio-answers input[type=radio]').prop('disabled',
				isIncorrectButton);
		var isAvatarChecked = $('#avatar-radio-answers input[type=radio]').is(
				':checked');
		if (isAvatarChecked) {
			_enableFinishButton(task, deferred);
		} else {
			_disableFinishButton();
		}
	}

	function _setupGUI(task, deferred) {
		resetComments();
		_disableFinishButton();
		$('#avatar-radio-answers input[type=radio]').prop('disabled', false);
		$('#avatar-radio-answers input[type=radio]').prop('checked', false);
		$('#ref-radio-answers input[type=radio]').prop('checked', false);
		$('#avatar-radio-answers input[type=radio]').off('click').on('click',
				function() {
					_handleAvatarRadioClick(this, task, deferred);
				});
		$('#ref-radio-answers input[type=radio]').off('click').on('click',
				function() {
					_handleRefRadioClick(this, task, deferred);
				});
		$('#skip-button').off('click').on('click', function() {
			_readAnswer(task, deferred, true);
		});
	}

	function _readAnswer(task, deferred, hasSkipped) {
		var status = '';
		var avatarAnswer = $('#avatar-radio-answers input[type=radio]:checked')
				.val();
		var refAnswer = $('#ref-radio-answers input[type=radio]:checked').val();
		if (hasSkipped) {
			status = 'SKIPPED';
		} else if (refAnswer == 'INCORRECT') {
			status = 'REF_DISAPPROVED';
		} else if (avatarAnswer == 'INCORRECT') {
			status = 'AVATAR_DISAPPROVED';
		} else {
			status = 'AVATAR_APPROVED';
		}
		_submitAnswer(task, deferred, status, hasSkipped);
	}

	function _createAnswer(task, status) {
		var answer = {};
		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != 'undefined';
		if (hasLastAnswer) {
			answer = lastAnswer;
		} else {
			answer = {
				'number_of_approval' : 0,
				'number_of_avatar_disapproval' : 0,
				'number_of_ref_disapproval' : 0
			};
		}
		answer['status'] = status;
		answer['comment_avatar'] = '';
		answer['comment_ref'] = '';
		if (status == 'AVATAR_APPROVED') {
			answer['number_of_approval'] = answer.number_of_approval + 1;
		} else if (status == 'AVATAR_DISAPPROVED') {
			answer['number_of_avatar_disapproval'] = answer.number_of_avatar_disapproval + 1;
			var commentAvatar = $('#comment-avatar textarea').val();
			if (commentAvatar.trim() != '') {
				answer['comment_avatar'] = commentAvatar;
			}
		} else if (status == 'REF_DISAPPROVED') {
			answer['number_of_ref_disapproval'] = answer.number_of_ref_disapproval + 1;
			var comment_ref = $('#comment-ref textarea').val();
			if (comment_ref.trim() != '') {
				answer['comment_ref'] = comment_ref;
			}
		}
		return answer;
	}

	function _finishTask(task, deferred, answer) {
		_enableLoading();
		$
				.ajax({
					type : 'POST',
					url : serverBackendUrl + '/finish_task',
					data : {
						'task_id' : task.id,
						'project_id' : task.project_id,
						'sign_name' : task.info.sign_name,
						'number_of_approval' : answer.number_of_approval,
						'number_of_avatar_disapproval' : answer.number_of_avatar_disapproval,
						'number_of_ref_disapproval' : answer.number_of_ref_disapproval
					},
					success : function(response) {
						pybossa.saveTask(task.id, answer).done(function() {
							_disableLoading();
							$('#success').fadeIn(500);
							$('#validador-container').hide();
							setTimeout(function() {
								deferred.resolve();
							}, 2000);
						});
					},
					error : function(xhr, textStatus, error) {
						_disableLoading();
						alert(xhr.responseText);
					}
				});
	}

	function _submitAnswer(task, deferred, status, hasSkipped) {
		var answer = _createAnswer(task, status);
		if (hasSkipped) {
			_saveAnswer(task, deferred, answer);
		} else {
			_finishTask(task, deferred, answer);
		}
	}

	function _saveAnswer(task, deferred, answer) {
		pybossa.saveTask(task.id, answer).done(function() {
			$('#success').fadeIn(500);
			$('#validador-container').hide();
			setTimeout(function() {
				deferred.resolve();
			}, 2000);
		});
	}

	function _loadTaskInfo(task) {
		currentTaskId = task.id;
		var signName = task.info.sign_name;
		var relVideoAvatarUrl = task.info.video_ava;
		var relVideoRefUrl = task.info.video_ref;
		var avatarVidUrl = apiDBHostUrl + relVideoAvatarUrl;
		var refVidUrl = apiDBHostUrl + relVideoRefUrl;

		$('.sign-label').text(signName);
		$('#avatar-video').html(videoHelper.getSourceByWebmUrl(avatarVidUrl));
		$('#ref-video').html(videoHelper.getSourceByWebmUrl(refVidUrl));
	}

	function _loadMainComponents() {
		pybossaApiHelper.setup(pybossaEndpoint, projectName);
		loadHtmlHelper.setup(baseUrl);
		ranking.setup(baseUrl, pybossaEndpoint, projectName, _getLoggedUser());
	}

	pybossa.presentTask(function(task, deferred) {
		_loadMainComponents();
		if (!$.isEmptyObject(task) && currentTaskId != task.id) {
			_loadTaskInfo(task);
			_setupGUI(task, deferred);
			$('#success').hide();
			$('#validador-container').fadeIn(500);
		} else {
			$('#validador-container').hide();
			$('#finish').fadeIn(500);
			ranking.tasksEnded();
		}
	});

	// Private methods
	function _run(projectname) {
		pybossa.setEndpoint(pybossaEndpoint);
		pybossa.run(projectname);
	}

	// Public methods
	validador.run = function(serverhost, serverbackend, projname, apidbhost) {
		baseUrl = serverhost;
		serverBackendUrl = serverbackend;
		apiDBHostUrl = apidbhost;
		pybossaEndpoint = '/pybossa';
		projectName = projname;
		_run(projectName);
	};

}(window.validador = window.validador || {}, jQuery));