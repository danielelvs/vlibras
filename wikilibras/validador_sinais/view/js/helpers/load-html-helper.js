(function(loadHtmlHelper, $, undefined) {
	
	var baseUrl = '';
	var templatesUrl = '';
	
	function _preprocessHtml(data) {
		return data.replace(/{{ server }}/g, baseUrl);
	}
	
	loadHtmlHelper.load = function(target, url, callback) {
		var completeUrl = templatesUrl + url;
		$.get(completeUrl, function(data) {
			$(target).html(_preprocessHtml(data));
			callback && callback();
		});
	}
	
	loadHtmlHelper.setup = function(url) {
		baseUrl = url;
		templatesUrl = baseUrl + "/templates";
	};

}(window.loadHtmlHelper = window.loadHtmlHelper || {}, jQuery));
