var parameters = require('../helpers/parameters');
var http = require('http');
var fs = require('fs');
var _ = require('lodash');

/*
* Função que processa o vídeo (seja baixando, seja pegando o vídeo enviado)
* Deve retornar um objeto contendo o nome e o caminho
*/
function downloadAndMoveVideo(req, folder, callback)
{
    console.log(req.files[0].path + " " + req.files[0].originalname);
    // Se enviou o arquivo na requisição
    if (req.files[0].fieldname !== undefined)
    {
        // Se a validação falhar
        if (parameters.checkVideo(req.files[0].originalname) === false)
        {
            var error = 'Vídeo enviado com extensão inválida';
            return callback(error);
        }
        /* Move o vídeo submetido para a pasta com o seu ID correspondente */
        try
        {
            if (!_.isEmpty(req.body.wikilibras))
            {
                fs.renameSync(req.files[0].path, 'avatar/' + req.files[0].originalname);
                fs.renameSync(req.files[1].path, 'blender/' + req.files[1].originalname);
            }
            else
            {
                fs.renameSync(req.files[0].path, 'uploads/' + req.files[0].originalname);
            }
        }
        catch (err)
        {
            console.log("Erro ao mover o vídeo submetido: " + err);
            callback("Erro ao mover o vídeo submetido: " + err);
        }
        return callback();
    } // Se o arquivo não foi enviado, mas um video_url foi
    else if (req.body.video_url !== undefined)
    {
        // Requisição para baixar o vídeo
        http.get(req.body.video_url, function(response)
        {

            // Se o vídeo não foi baixado com sucesso
            if (response.statusCode !== 200)
            {
                var error = 'Problema ao carregar video_url: status ' + response.statusCode;
                return callback(error);
            }

            // Nome do arquivo
            var filename = req.body.video_url.substring(req.body.video_url.lastIndexOf('/') + 1);

            // Tira os parâmetros HTTP
            if (filename.lastIndexOf("?") !== -1)
            {
                filename = filename.substring(0, filename.lastIndexOf("?"));
            }

            var path = folder + '/' + filename;

            // Cria o stream para escrita
            var file = fs.createWriteStream(path);

            // Salva o arquivo em disco
            response.pipe(file);

            // Quando a escrita acabar
            file.on('finish', function()
            {
                // Fecha o arquivo
                file.close(function()
                {

                    // Retorna o vídeo baixado
                    locals.video = {
                        'path': path
                    };

                    // Chama o callback para prosseguir execução
                    callback();
                });
            });

        // Se deu erro na requisição de baixar o vídeo
        }).on('error', function(e) {
            var error = 'Problema ao carregar video_url: ' + e.message;
            return callback(error);
        });

    // Se nem o vídeo foi enviado e nem o video_url foi preenchido
    }
    else
    {
        var error = "Video deve ser enviado como parâmetro 'video' ou como 'video_url'";
        return callback(error);
    }
}

module.exports.downloadAndMoveVideo = downloadAndMoveVideo;
