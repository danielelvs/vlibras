(function () {
    'use strict';

    angular.module('wikilibras.ranking').service("rankingService", ['$http', '$q', function ($http, $q) {
        var PROJECT_CONF_URL = 'conf/app-conf.json';
        var RANKING_ENDPOINT =  '/api/leaderboard?limit=20';
        var UPLOADED_SIGNS_RANKING_ENDPOINT = '/countuservideos?limit=20';
        var NUMBER_OF_COLUMNS = 2;

        function splitToChunks(array, chunk) {
            if (array && array.length < 1 || chunk === 0) return array;

            var result = [];
            for (var i = 0; i < array.length; i += chunk) {
                result.push(array.slice(i, i+chunk));
            }
            return result;
        }

        return {
            getRankingData: function() {
                return $http.get(PROJECT_CONF_URL).then(function(response) {
                    var baseApiUrl = response.data.pybossa_url;
                    var baseDBApiUrl = response.data.db_api_url;

                    return $q.all([$http.get(baseApiUrl + RANKING_ENDPOINT), $http.get(baseDBApiUrl + UPLOADED_SIGNS_RANKING_ENDPOINT)]).then(function(response) {
                        if (typeof response[0] === 'undefined' ||
                                typeof response[1] === 'undefined') return;
                        response[0].data['uploaded_signs'] = response[1].data;
                        response = response[0];

                        angular.forEach(response.data, function(obj, key) {
                            response.data[key] = splitToChunks(obj, NUMBER_OF_COLUMNS);
                        });
                        console.log(response.data);
                        return response.data;
                    });
                });
            }
        };
    }]);
}());