(function () {
    'use strict';

    angular.module('wikilibras.progress').service("progressService", ['$http', '$q', function ($http, $q) {
        var PROJECT_CONF_URL = 'conf/app-conf.json';
        var PROJECTS_PROGRESS_ENDPOINT = '/api/projects_progress';
        var UPLOADED_SIGNS = '/countVideo';

        function accessGoalsData(goals, projectName, param) {
            var projectGoals = goals[projectName];
            if (!projectGoals || typeof projectGoals === 'undefined') return '';
            var goal = projectGoals[param];
            if (!goal || typeof goal === 'undefined') return '';
            return goal;
        }

        return {
            getProjectsProgressData: function() {
                return $http.get(PROJECT_CONF_URL).then(function(response) {
                    var basePyBossaApiUrl = response.data.pybossa_url;
                    var baseDBApiUrl = response.data.db_api_url;
                    var responseGoals = response.data.goals;

                    return $q.all([
                        $http.get(basePyBossaApiUrl + PROJECTS_PROGRESS_ENDPOINT),
                        $http.get(baseDBApiUrl + UPLOADED_SIGNS)
                        ]).then(function(response) {
                            var responseProgress = response[0].data;
                            var uploadSignsProgress = {
                                'short_name': 'uploaded_signs',
                                'n_completed_tasks': response[1].data[0]
                            };
                            responseProgress.push(uploadSignsProgress);

                            angular.forEach(responseProgress, function(progress) {
                                progress['goal_n_tasks'] = accessGoalsData(responseGoals, progress.short_name, 'goal_n_tasks');
                                progress['goal_deadline'] = moment(accessGoalsData(responseGoals, progress.short_name, 'goal_deadline'), "DD/MM/YYYY");
                                progress['last_activity'] = moment(progress['last_activity']);
                            });

                            console.log(responseProgress);

                            return responseProgress;
                        });
                });
            }
        };
    }]);
}());