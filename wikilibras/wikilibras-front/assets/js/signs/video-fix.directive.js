(function () {
    'use strict';

    angular.module('wikilibras.signs').directive('html5vfix', ['$sce', function($sce) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                attr.$set('src', $sce.trustAsResourceUrl(attr.vsrc));
                document.getElementById("sign-modal-video").load();
            }
        }
    }]);
}());