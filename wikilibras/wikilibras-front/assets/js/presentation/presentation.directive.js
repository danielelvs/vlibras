(function () {
    'use strict';

    angular.module('wikilibras.presentation').directive('presentationModal', ['$window', function($window) {
        return {
            restrict: 'E',
            templateUrl: 'assets/js/presentation/presentation-modal.html',
            link: function(scope, element, attr) {
                var player = undefined;

                function loadVideo() {
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                }
                $window.onYouTubeIframeAPIReady = function() {
                    player = new YT.Player(document.getElementById('presentation-player'), {
                        videoId: 'lKA-QsdeHFo',
                        events: {
                            'onReady': setupPlayer
                        }
                    });
                };
                function setupPlayer() {
                    $('.presentation-modal').off('hidden.bs.modal').on('hidden.bs.modal', function () {
                        player.stopVideo();
                    });
                    $('.wl-video-control').off('click').on('click', function() {
                        player.playVideo();
                        $('.presentation-modal').modal('show');
                    });
                };
                loadVideo();
            }
        }
    }]);
}());