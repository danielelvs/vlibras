# -*- coding: UTF-8 -*-

import bpy
import json
import sys

def main():
    if (len(sys.argv) == 8):
        json_obj = json.loads(sys.argv[7])
        try:
            action_name = json_obj["action_name"]
            action_fake_is_valid = json_obj["action_fake_is_valid"]
            min_frame_count =  json_obj["min_frame_count"]
        except:
            sys.exit(4)
        action = None
        for i in bpy.data.actions:
            # ignore action name
            #if (str(i.name.upper()) == str(action_name)):
            #    pass
            action = i
            break
        if (action == None):
            sys.exit(5)
        if not (action_fake_is_valid):
            if (action.use_fake_user):
                sys.exit(6)
        if ((bpy.context.scene.frame_end - bpy.context.scene.frame_start) < min_frame_count):
            sys.exit(7)
    else:
        sys.exit(8)

if __name__ == "__main__":
    main()
