# -*- coding: UTF-8 -*-

import bpy
import json
import os
import shutil
import sys

def configure_output():
    bpy.context.scene.frame_start = 0
    bpy.context.scene.frame_current = 0
    bpy.context.scene.render.resolution_x = 640
    bpy.context.scene.render.resolution_y = 480
    bpy.context.scene.render.resolution_percentage = 100
    bpy.context.scene.render.image_settings.file_format = 'H264'
    bpy.context.scene.render.ffmpeg.format = 'MPEG4'
    bpy.context.scene.render.ffmpeg.codec = 'H264'
    bpy.context.scene.render.use_shadows = False
    bpy.context.scene.render.use_raytrace = False
    bpy.context.scene.render.use_envmaps = False
    bpy.context.scene.render.use_motion_blur = False
    bpy.context.scene.render.use_shadows = False
    return

def file_rename(file_full_path):
    file_path = os.path.dirname(os.path.abspath(file_full_path))
    filename = os.path.basename(os.path.splitext(file_full_path)[0])
    extension = os.path.splitext(file_full_path)[1]
    filename_reversed = ""
    valid_char = False
    for char in reversed(filename):
        if (valid_char == True):
            filename_reversed += char
        if (char == "_"):
            valid_char = True
    try:
        filename_reversed = filename_reversed[::-1]
        new_filename = os.path.join(file_path, "%s%s" % (filename_reversed, extension))
        shutil.move(file_full_path, new_filename)
        return new_filename
    except Exception:
        return ""

def render_video(upload_dir, sign_name):
    bpy.context.scene.render.filepath = os.path.join(upload_dir, sign_name + "_")
    try:
        bpy.ops.render.render(animation = True, write_still = False, layer = "", scene = "")
        return str("%s%0.4i-%0.4i.mp4" % (bpy.context.scene.render.filepath, bpy.context.scene.frame_start, bpy.context.scene.frame_end))
    except:
        return ""

def main():
    if (len(sys.argv) == 8):
        json_obj = json.loads(sys.argv[7])
        try:
            upload_dir = json_obj["upload_dir"]
            sign_name = json_obj["sign_name"]
        except:
            sys.exit(3)
        try:
            configure_output()
            file_rename(render_video(upload_dir, sign_name))
        except:
            sys.exit(4)
    else:
        sys.exit(5)

if __name__ == "__main__":
    main()
