### Configuração de Instalação

#### Copie o arquivo modelo de configuração

```sh
$ cp settings_local.json.template settings_local.json
```

#### Edite o arquivo de configuração

```sh
$ gedit settings_local.json
```

#### Substitua os valores: localhost, my-api-key
```json
{
    "db_host": "localhost",
    "pb_api_key": "my-api-key",
    "pb_endpoint": "http://localhost/pybossa/"
}
```

#### Exemplo:
```json
{
    "db_host": "192.168.1.100",
    "pb_api_key": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
    "pb_endpoint": "http://192.168.1.100/pybossa/"
}
```

### Instalação de dependências

```sh
$ make install
```

#### Execute teste (Opcional)

```sh
$ make run
```

#### Habilitar a inicialização automática do serviço

```sh
$ make autostart-enable
```

#### Desabilitar a inicialização automática do serviço

```sh
$ make autostart-disable
```
