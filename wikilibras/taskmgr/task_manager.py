# -*- coding: UTF-8 -*-

import json
import pbclient
import pyutil
import requests
import logging


class task_creator():
    def __init__(self, json_data):
        self.data = {}
        self.data["db_host"] = json_data.get("db_host", "localhost")
        self.data["db_timeout"] = json_data.get("db_timeout", 30)
        self.data["db_port"] = json_data.get("db_port", 200)
        self.data["pb_api_key"] = json_data.get("pb_api_key", "")
        self.data["pb_endpoint"] = json_data.get("pb_endpoint", "http://localhost/pybossa/")
        self.data["corretor"] = dict(json_data.get("corretor", {}))
        self.data["validador"] = dict(json_data.get("validador", {}))
        self.data["wikilibras"] = dict(json_data.get("wikilibras", {}))
        self.set_pb_config(self.data["pb_api_key"], self.data["pb_endpoint"])
        self.data["corretor"]["project_id"] = self.get_pb_project_id(self.data["corretor"]["short_name"])
        self.data["validador"]["project_id"] = self.get_pb_project_id(self.data["validador"]["short_name"])
        self.data["wikilibras"]["project_id"] = self.get_pb_project_id(self.data["wikilibras"]["short_name"])
        logging.getLogger("requests").setLevel(logging.ERROR)

    def set_pb_config(self, pb_api_key, pb_endpoint):
        pbclient.set('api_key', self.data["pb_api_key"])
        pbclient.set('endpoint', self.data["pb_endpoint"])

    def get_pb_project_id(self, pb_short_name):
        projects = pbclient.find_project(short_name=pb_short_name)
        if (len(projects) > 0):
            return projects[0].id
        else:
            return None

    def to_string(self):
        return json.dumps(self.data, sort_keys=True, ensure_ascii=False, indent=4)

    def __get__(self, endpoint):
        try:
            r = requests.get(('http://%s:%s/%s' % (self.data["db_host"], self.data["db_port"], endpoint)), timeout=self.data["db_timeout"])
            return r
        except:
            return {}

    def get_newtasks(self, selo=None):
        url = "newtasks?task_manager_alive=true"
        if (isinstance(selo, int)):
            url += "?selo=%d" % (selo)
        r = self.__get__(url)
        if (r != {}):
            # pyutil.log(json.dumps(r.json(), ensure_ascii = False, indent = 4))
            return r.json()
        else:
            return []

    def run(self):
        try:
            dbquery = self.get_newtasks()
            tasks = []
            if (dbquery == []):
                print("Wikilibras DB API not running at 'http://%s:%s'" % (self.data["db_host"], self.data["db_port"]))
                return
            if (dbquery["status"]):
                tasks = dbquery["data"]
            else:
                pyutil.log("DecodeError: " + str(dbquery))
                return
            for i in tasks:
                id_selo = i["idSelo"]
                nome_sinal = i["nome"]
                id_sinal = i["idSinal"]
                usuario = i["usuario"]
                estado = i["estado"]
                classe = i["classe"]
                blend_file = i["blender"]
                video_ref = i["file"]
                video_ava = i["avatar"]

                proj_name = None
                proj_id = None

                if (id_selo == 7):
                    proj_name = "wikilibras"
                elif (id_selo == 4):
                    proj_name = "corretor"
                elif (id_selo in [1, 5]):
                    proj_name = "validador"
                else:
                    continue

                proj_id = self.data[proj_name]["project_id"]
                public_users = "/public/users"

                if (usuario is not None):
                    public_users += "/" + usuario
                if (estado is not None):
                    public_users += "/" + estado
                if (classe is not None):
                    public_users += "/" + classe

                task = dict(
                    sign_name=nome_sinal,
                    submission_date=pyutil.get_date_now()
                )

                if ((video_ref is not None) and (id_selo in [4, 1, 5, 7])):
                    task["video_ref"] = ("%s/%s" % (public_users, video_ref))

                if ((video_ava is not None) and (id_selo in [4, 1, 5])):
                    task["video_ava"] = ("%s/%s" % (public_users, video_ava))

                if ((blend_file is not None) and (id_selo in [4])):
                    task["blend"] = ("%s/%s" % (public_users, blend_file))

                # print(repr(task).decode("unicode-escape"))
                # print(json.dumps(task, ensure_ascii=True, indent=4, sort_keys=False))
                response_task = pbclient.create_task(proj_id, task)
                pyutil.log("[%s] created task[%d] '%s'" % (proj_name, int(response_task.id), nome_sinal))
                # TODO
                # verificar se o sinal foi atualizado corretamente,
                # caso contrario persistir lista com (id_sinal, task_id)
                # para atualizar posteriormente
                self.__get__("updatetask?idsinal=%d&idtask=%d" % (int(id_sinal), int(response_task.id)))
        except:
            pyutil.print_stack_trace()
