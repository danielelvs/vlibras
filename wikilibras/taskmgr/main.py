# -*- coding: UTF-8 -*-

import json
import os
import pyutil
import sys
import task_manager
import time

def load_json(filename):
    r = {}
    try:
        fp = open(filename, "r")
        if (fp):
            r = json.load(fp)
        fp.close()
    except:
        pyutil.print_stack_trace()
    return r

def save_json(data, filename, idt = 4):
    try:
        fp = open(filename, "w")
        if (fp):
            json.dump(
                data,
                fp,
                ensure_ascii = False,
                indent = idt,
                sort_keys = True
            )
        fp.close()
    except:
        pyutil.print_stack_trace()
        return False
    return True

def main():
    seconds = 10
    json_data = None
    task_creator = None
    pyutil.log("wikilibras task manager started")
    while (task_creator == None):
        try:
            json_data = load_json(os.path.join(os.path.dirname(os.path.abspath("__file__")), "settings_local.json"))
            os.path.dirname(os.path.abspath("__file__"))
            task_creator = task_manager.task_creator(json_data)
        except:
            task_creator = None
            print("Waiting for Network to Get Project ID")
            pyutil.print_stack_trace()
            time.sleep(10)
    if (json_data != None and task_creator != None):
        # print(task_creator.to_string())
        pyutil.log("wikilibras task manager checking for tasks")
        while (True):
            task_creator.run()
            # print("Waiting %d seconds to check new tasks" % (seconds))
            time.sleep(seconds)

if __name__ == "__main__":
    main()
