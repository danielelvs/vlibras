import bpy
import moves
import util

def facial(js_facial, current_frame, frame_jump):
    return moves.facial(js_facial, current_frame, frame_jump)

def contato(js_movement, current_frame, frame_jump, is_right_hand):
    sub_type = next(iter(js_movement.keys()))
    mov_param = js_movement[sub_type]
    action = util.right_hand_actions if is_right_hand else util.left_hand_actions
    bones = util.right_bones_conf if is_right_hand else util.left_bones_conf
    current_frame = current_frame + (2 * frame_jump)
    return moves.contato(action, sub_type, mov_param, bones, is_right_hand, current_frame, frame_jump)

def hand_mov(current_frame, frame_jump, js_mao, is_right_hand):
    if (js_mao == {}):
        return current_frame + (2 * frame_jump)
    movement_name = next(iter(js_mao.keys()))
    if (movement_name == "circular"):
        current_frame = moves.circular(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    elif (movement_name == "semicircular"):
        current_frame = moves.semicircular(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    elif (movement_name == "contato"):
        current_frame = contato(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    elif (movement_name == "helicoidal"):
        current_frame = moves.helicoidal(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    elif (movement_name == "pontual"):
        current_frame = moves.pontual(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    elif (movement_name == "retilineo"):
        current_frame = moves.retilineo(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    elif (movement_name == "senoidal"):
        current_frame = moves.senoidal(js_mao[movement_name], current_frame, frame_jump, is_right_hand)
    return current_frame + frame_jump
