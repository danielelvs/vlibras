# -*- coding: UTF-8 -*-

import datetime
import logging
import os
import shutil
import sys

# @def funcao para obter data e hora atual do sistema
# @param string formato de data e hora
# @return string retorna data e hora do sistema no momento da chamada
def getTimeStamp(date_fmt = "%Y-%m-%d %H:%M:%S.%f"):
    if ("%f" in date_fmt):
        # [:-3] remove 3 casas decimais dos milisegundos (ms)
        return datetime.datetime.now().strftime(date_fmt)[:-3]
    else:
        return datetime.datetime.now().strftime(date_fmt)

# @def funcao para gravar log dos eventos em arquivo
# @param string mensagem a ser salva
# @param int indice do tipo de log 0: apenas print, 1: debug, 2: info, 3: warn, 4: error, 5: critical
# @param string caminho completo do arquivo de logs
# @param string formato de tempo utilizado
# @return none
def log(msg = "", log_level = 2, log_file = "events.log"):
    dict_level = {
        0: ["Print",    None,             None],
        1: ["DEBUG",    logging.DEBUG,    logging.debug],
        2: ["INFO",     logging.INFO,     logging.info],
        3: ["WARNING",  logging.WARN,     logging.warn],
        4: ["ERROR",    logging.ERROR,    logging.error],
        5: ["CRITICAL", logging.CRITICAL, logging.critical]
    }
    # log_format = "[%(asctime)s.%(msecs).03d] %(levelname)s: <User: %(name)s> <Module: %(module)s> <Function: %(funcName)s>: %(message)s"
    log_format = "[%(asctime)s.%(msecs).03d] %(levelname)s: %(message)s"
    date_fmt = "%Y-%m-%d %H:%M:%S"
    logging.basicConfig(filename = log_file, datefmt = date_fmt, format = log_format, level = dict_level[log_level][1])
    logging.Formatter(fmt = "%(asctime)s", datefmt = date_fmt)
    log_level %= len(dict_level)
    write_mode = dict_level[log_level][2]
    print("[%s] %s: %s" % (getTimeStamp(), dict_level[log_level][0], msg))
    if (write_mode != None):
        write_mode(msg)
    return

# @def funcao para exibir excecao
# @param string deve ser passado: "__file__" para identificar em qual modulo ocorreu a excecao
# @return int retorna 1
def print_stack_trace():
    error = "\n    File name: %s\n    Function name: %s\n    Line code: %s\n    Type exception: %s\n    Message: %s" % (
            os.path.basename(sys.exc_info()[2].tb_frame.f_code.co_filename),
            sys.exc_info()[2].tb_frame.f_code.co_name,
            sys.exc_info()[2].tb_lineno,
            sys.exc_info()[0].__name__,
            sys.exc_info()[1]
    )
    log(error, 4)
    return 1

# @def funcao que verifica se um arquivo existe
# @param string caminho do arquivo a ser checado
# @return bool verdadeiro se o arquivo existir, falso caso contrario
def file_exists(file_path):
    if ((os.path.isfile(file_path) == 1) and (os.path.exists(file_path) == 1)):
        return True
    else:
        return False

# @def funcao para renomear arquivo de video gerado pelo blender
#      entrada: /temp/arquivo_video_0001-0250.mp4
#      saida:   /temp/arquivo_video.mp4
# @param string caminho do arquivo a ser renomeado
# @param bool renomeia o arquivo de acordo com a data e hora do sistema
# @param bool sobreescreve o arquivo
# @return string retorna o nome do arquivo renomeado
def file_rename(file_full_path, use_time_stamp = False, overwrite = True):
    if (file_exists(file_full_path) == False):
        return ""
    file_path = os.path.dirname(os.path.abspath(file_full_path))
    filename = os.path.basename(os.path.splitext(file_full_path)[0])
    extension = os.path.splitext(file_full_path)[1]
    filename_reversed = ""
    valid_char = False
    # percorre o "filename" do final para o inicio copiando caracteres apos o primeiro "_" encontrado
    for char in reversed(filename):
        if (valid_char == True):
            filename_reversed += char
        if (char == "_"):
            valid_char = True
    try:
        # inverte sequencia de caracteres que foi copiada
        filename_reversed = filename_reversed[::-1]
        if (use_time_stamp == True):
            # recupera novo file_full_path + nome do arquivo + data/hora + extensao
            new_filename = os.path.join(file_path, "%s_%s%s" % (filename_reversed, getTimeStamp(), extension))
        else:
            # recupera novo file_full_path + nome do arquivo + extensao
            new_filename = os.path.join(file_path, "%s%s" % (filename_reversed, extension))
            # Enumera o nome do arquivo caso o parametro "overwrite" seja "False" e o arquivo ja exista
            if (overwrite == False):
                count = 0
                while (file_exists(new_filename) == True):
                    count += 1
                    # new_filename = join(file_path, "%s_%i%s" % (filename_reversed, count, extension))
                    new_filename = os.path.join(file_path, "%s_%0.4i%s" % (filename_reversed, count, extension))
        # log('rename: "%s" to: "%s"' %(file_full_path, new_filename))
        shutil.move(file_full_path, new_filename)
        return new_filename
    except Exception:
        print_stack_trace()
        return ""

"""
# unit test
def main():
    msg = "This is a message of "
    log(msg + "Print", 0)
    log(msg + "Debug", 1)
    log(msg + "Info", 2)
    log(msg + "Warn", 3)
    log(msg + "Error", 4)
    log(msg + "Critical", 5)
    filename = "sample_001-250.mp4"
    video = open(filename, "w")
    video.write
    video.close
    file_rename(filename)
    return

if __name__ == "__main__":
    main()
"""