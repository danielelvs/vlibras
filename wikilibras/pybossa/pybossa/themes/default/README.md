This is a custom theme for the [Pybossa server](https://github.com/PyBossa/pybossa) created for [Contribua.org](https://contribua.org/).

![UFCG](http://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/UfcgBrasao.jpg/200px-UfcgBrasao.jpg)

![LSD](http://novolsd.lsd.ufcg.edu.br/wp-content/themes/lsd_theme/images/logo_es.gif)

Contribua is a fork of Pybossa crowdsourcing engine. It can be used for any distributed tasks project
but was initially developed to help scientists and other researchers
crowd-source human problem-solving skills!

## See it in Action

[Contribua.org](https://contribua.org/)