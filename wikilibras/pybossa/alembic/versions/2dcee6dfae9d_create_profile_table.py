"""create profile table

Revision ID: 2dcee6dfae9d
Revises: 4f12d8650050
Create Date: 2016-05-24 14:30:22.687206

"""

# revision identifiers, used by Alembic.
revision = '2dcee6dfae9d'
down_revision = '4f12d8650050'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ARRAY
import datetime


def make_timestamp():
    now = datetime.datetime.utcnow()
    return now.isoformat()


def upgrade():
    op.create_table(
        'profile',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.Text, nullable=False, unique=True),
        sa.Column('description', sa.Text, nullable=False),
        sa.Column('access', ARRAY(sa.Text), nullable=False),
        sa.Column('created', sa.Text, server_default=make_timestamp()),
    )
    
    # Add two categories
    query = 'INSERT INTO profile (name, description, access) VALUES (\'Colaborador\', \'Colaborador geral\', \'{wikilibras}\')'
    op.execute(query)
    query = 'INSERT INTO profile (name, description, access) VALUES (\'Animador\', \'Animador 3D\', \'{wikilibras, corretor_sinais}\')'
    op.execute(query)
    query = 'INSERT INTO profile (name, description, access) VALUES (\'Especialista\', \'Especialista em LIBRAS\', \'{wikilibras, validador_sinais}\')'
    op.execute(query)

    op.add_column('user', sa.Column('profile_id', sa.Integer, sa.ForeignKey('profile.id'), server_default="1"))
    

def downgrade():
    op.drop_column('user', 'profile_id')
    op.drop_table('profile')
    