-- how to install:
-- sudo su postgres -c "psql --set ON_ERROR_STOP=off -f wikilibras-pybossa.sql pybossa"

INSERT INTO "user" VALUES (1, '2017-01-01T00:00:00.000000', 'wikilibras@lavid.ufpb.br', 'wikilibras', 'wikilibras', 'pt_BR', '2324bc23-7d6f-4840-8905-b1e6c1675eed', 'pbkdf2:sha1:1000$wIP6vkOx$99be5c325961aa39030bb10e3b58a85ac3bfaa90', true, false, false, NULL, NULL, NULL, NULL, NULL, NULL, false, true, false, true, '{}', 1);
SELECT pg_catalog.setval('user_id_seq', 1, true);

INSERT INTO "user" VALUES (2, '2017-01-01T00:00:00.000000', 'tester1@lavid.ufpb.br',    'tester1',    'tester1',    'pt_BR', 'e4fef6e0-d954-4ce8-be39-ab08a96aa61a', 'pbkdf2:sha256:50000$ZXnRVTb5$2bfd28d77b26d3c1f183192ec793885bbd523ad92b8a6708edce6d5226a850fe', true, false, false, NULL, NULL, NULL, NULL, NULL, NULL, false, true, false, true, '{}', 1);
INSERT INTO "user" VALUES (3, '2017-01-01T00:00:00.000000', 'tester2@lavid.ufpb.br',    'tester2',    'tester2',    'pt_BR', '1d0fbea5-88d7-40f2-a835-676bc3824f8c', 'pbkdf2:sha256:50000$DizF7nb3$d4b4946fcac8a88cb2337d610e743432441719559f236b0d5816feb9ffbbefdf', true, false, false, NULL, NULL, NULL, NULL, NULL, NULL, false, true, false, true, '{}', 1);
INSERT INTO "user" VALUES (4, '2017-01-01T00:00:00.000000', 'tester3@lavid.ufpb.br',    'tester3',    'tester3',    'pt_BR', '8c5707aa-e813-4a5e-b3f1-7d3e413da494', 'pbkdf2:sha256:50000$pYfjlhqL$a3bbd613e39c0ef6387548b4ef1c8b273bf02031b34b10d2f26f21f0839da890', true, false, false, NULL, NULL, NULL, NULL, NULL, NULL, false, true, false, true, '{}', 1);
INSERT INTO "user" VALUES (5, '2017-01-01T00:00:00.000000', 'tester4@lavid.ufpb.br',    'tester4',    'tester4',    'pt_BR', 'f47796e1-e0da-49ff-b030-ad7459cf464b', 'pbkdf2:sha256:50000$Zht8o4X9$b4e630fb43254589c407406836bf60b5e669f7368dfa45a24ebbc0ed5fee3872', true, false, false, NULL, NULL, NULL, NULL, NULL, NULL, false, true, false, true, '{}', 1);
INSERT INTO "user" VALUES (6, '2017-01-01T00:00:00.000000', 'tester5@lavid.ufpb.br',    'tester5',    'tester5',    'pt_BR', '7dce7aac-29cd-467f-a1b6-cc96044b5909', 'pbkdf2:sha256:50000$v6cDxeNk$2f7b494d9d4b2e175cdfc9e617758a3a6c30e511385bc24e4d1b499064f9a908', true, false, false, NULL, NULL, NULL, NULL, NULL, NULL, false, true, false, true, '{}', 1);
SELECT pg_catalog.setval('user_id_seq', 6, true);
