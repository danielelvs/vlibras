var uuid = require('node-uuid');
var API = require('../db/api');

function init_endpoint(req, res, Sign, Status) {
	/* Verifica se o paramêtro [name] existe ou possui algum valor */
	if ((req.body.userId === '') || (req.body.userId === undefined)) {
		res.send(500, { 'error': 'Especifique o usuário'});
		return;
	}

	var sign_object = new Sign({
		name: req.body.userId,
		uuid: uuid.v4(),
		created_at: new Date(),
		updated_at: new Date(),
		status: Status
	});

	API.create(sign_object, function(result) {
		if (result !== null) {
			res.send(200, { 'status': 'Sinal ' + result.name + ' criado com sucesso'});
		} else {
			res.send(500, { 'error': 'Erro na criação.'});
		}
	});
};

module.exports.init = init_endpoint;

