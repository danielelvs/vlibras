/* Helpers */
var properties = require('./helpers/properties');

/* Endpoints */
var endpoint_sinal = require('./endpoints/sinal');

/* Environment */
var fs = require('fs');
var https = require('https');
var http = require('http');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var PythonShell = require('python-shell');
var cors = require('cors');

var app = express();
app.use(cors())

/* SSL configuration */
var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

var options = {
	pythonPath: 'python3',
  	scriptPath: __dirname + '/../wikilibras-core',
  	args: []
};

/* Database */
var mongoose = require('mongoose');
require('./db/config').connect(mongoose);
var Sign = require('./db/schemas/sinal').init(mongoose);
var API = require('./db/api');
var outputVideosDir = __dirname + '/../wikilibras-core/users/'

app.use("/public", express.static(outputVideosDir));

app.use(express.urlencoded({ keepExtensions: true }))
app.use(express.json())

app.get('/', function(req, res) {
	res.send(200, { 'status': 'server is running!' });
});

app.get('/signs', function(req, res) {
	res.sendfile('views/index.html');
});

app.post('/sign', function(req, res) {
	options.args = JSON.stringify(req.body);
	PythonShell.run('controller.py', options, function (err, results) {
  		if (err) { console.log(err); endpoint_sinal.init(req, res, Sign,"Falhou"); res.send(400); return; }
        // results is an array consisting of messages collected during execution
		endpoint_sinal.init(req, res, Sign,"Sucesso");
		res.send(200);
	});
});

app.get('/api/signs', function(req, res) {
	API.read_all(Sign, function(result) {
		if (result !== null) {
			res.send(200, result);
		} else {
			res.send(500, { 'error': 'Erro na busca.'});
		}
	});
});

app.delete('/api/:hash', function(req, res) {
	API.remove(Sign, req.params.hash, function(result) {
		if (result !== null) {
			res.send(200, { 'status': 'Remoção concluída.'});
		} else {
			res.send(500, { 'error': 'Erro na remoção.'});
		}
	});
});

function createHttpsServer() {
	var httpsServer = https.createServer(credentials, app);
	httpsServer.listen(properties.PORT, properties.HOST, function() {
		console.log('Server running on https://' + properties.HOST + ':' + properties.PORT);
	});
}

function createHttpServer() {
        var httpServer = http.createServer(app);
        httpServer.setTimeout(properties.CONNECTION_TIMEOUT);
        httpServer.listen(properties.PORT, properties.HOST,  function() {
                console.log('Server running on http://' + properties.HOST + ':' + properties.PORT);
        });
}

if (properties.SSL) {
	createHttpsServer();
} else {
	createHttpServer();
}
