(function(defaultConfigurationHandler, $, undefined) {

  defaultConfigurationHandler.setup = function(hand, subConfig, step) {
    var baseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
			subConfig + '][step=' + step + ']';
    $(baseId + ' .selection-panel-option').off('click').on(
        'click', function() {
          selectionPanel.selectAnOption(baseId, this);
          dynworkflow.userSelectedAnOption();
        });
  };
  
  function _startVideoLoop(hand, subConfig, step, timeBetweenLoops) {
	setTimeout(function(){
	    $('.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
	    		subConfig + '][step=' + step + '] video').each(function(){
	    		videoHelper.play(this);
	    });
	    _startVideoLoop(hand, subConfig, step, timeBetweenLoops);
	}, timeBetweenLoops);
  }
  
  defaultConfigurationHandler.startVideoLoop = function(hand, subConfig, step, timeBetweenLoops) {
	  _startVideoLoop(hand, subConfig, step, timeBetweenLoops);
  }

}(window.defaultConfigurationHandler = window.defaultConfigurationHandler || {}, jQuery));
