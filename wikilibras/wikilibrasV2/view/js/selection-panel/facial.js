(function(facial, $, undefined) {

	facial.setup = function(subConfig) {
		var baseId = '.selection-panel-body[mainConfig=facial][subConfig='
				+ subConfig + ']';
		$(baseId + ' .selection-panel-option').off('click').on('click',
				function() {
					selectionPanel.selectAnOption(baseId, this);
					dynworkflow.userSelectedAnOption();
				});
		$(baseId + ' .video-panel-option').off('mouseenter').on('mouseenter',
				function(event) {
			$(this).addClass('video-panel-option-hover');
		});
		$(baseId + ' .video-panel-option').off('mouseleave').on('mouseleave',
				function(event) {
			$(this).removeClass('video-panel-option-hover');
		});
	};

}(window.facial = window.facial || {}, jQuery));
