(function(movement, $, undefined) {

  movement.getPreviousSelectedMovement = function(mainConfig) {
    return typeof mainConfig === "undefined" || mainConfig === "" ? "" : $('.selection-panel-body[mainConfig=' +
    		mainConfig + '][subConfig=movimento][step=1] .selection-panel-option[select=true]').attr('value');
  };

  movement.setup = function(serverhost, hand) {
    var baseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=movimento][step=1]';
    $(baseId + ' .selection-panel-option').off('click').on(
        'click', function() {
          selectionPanel.selectAnOption(baseId, this);
          dynworkflow.selectMovement($(this).attr('value'));
        });
	$(baseId + ' .video-panel-option').off('mouseenter').on('mouseenter',
			function(event) {
		$(this).addClass('video-panel-option-hover');
	});
	$(baseId + ' .video-panel-option').off('mouseleave').on('mouseleave',
			function(event) {
		$(this).removeClass('video-panel-option-hover');
	});
  };
}(window.movement = window.movement || {}, jQuery));
