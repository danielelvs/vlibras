(function(articulation, $, undefined) {

	var server_host = '';
	var MAX_COLUMNS = 14;

	function _updateASelector(container, ballSelector, step) {
		var pointSelector = parseInt(step) == 2 ? 'A' : 'B';
		$(container + ' .ball-selector.active').each(function() {
			$(this).removeClass('active');
			$(this).find('.point-selector').remove();
		});
		ballSelector.addClass('active');
		ballSelector.append('<div class="point-selector"><img src="'
				+ server_host + '/img/pa/' + pointSelector
				+ '-Seletor.png" class="point-selector" alt=""></div>');
		$(container + ' .selection-panel-option[select=true]').attr('select',
				false);
		$(ballSelector).attr('select', true);
	}

	function _getSelectedY(hand, subConfig, step) {
		step = parseInt(step) - 1;
		var previousStepId = '.selection-panel-body[mainConfig=' + hand
				+ '][subConfig=' + subConfig + '][step=' + step
				+ '] .module-x-y';
		return $(previousStepId).attr('data-y');
	}

	function _setupModuleZ(hand, subConfig, step, selectedY) {
		if (typeof selectedY == 'undefined' || selectedY == '')
			return;

		var base_id = '.selection-panel-body[mainConfig=' + hand
				+ '][subConfig=' + subConfig + '][step=' + step + ']';
		var articulation_z = base_id + ' .module-z';
		$(articulation_z + ' .ball-selector').hide();
		$(articulation_z + ' .row-number-' + selectedY + ' .ball-selector')
				.show();

		var z = $(articulation_z).attr('data-z');
		if (typeof z != 'undefined') {
			var ball_selector = $(articulation_z + ' .row-number-' + selectedY
					+ ' .ball-' + z);
			_updateASelector(articulation_z, ball_selector, step);
		}
	}

	function _setupBallSelectorXY(hand, subConfig, step) {
		var base_id = '.selection-panel-body[mainConfig=' + hand
				+ '][subConfig=' + subConfig + '][step=' + step + ']';
		var articulation_x_y = base_id + ' .module-x-y';
		$(articulation_x_y + ' .ball-selector')
				.off('click')
				.on(
						'click',
						function(a) {
							var b = $(a.target);
							if (!b.hasClass('ball-selector')) {
								dynworkflow.userSelectedAnOption();
								return;
							}
							var c = b.parent('.grid-row'), d = $(articulation_x_y), f = b
									.attr('data-x'), g = c.attr('data-y');
							d.attr('data-x', f), d.attr('data-y', g);

							var nextStep = parseInt(step) + 1;
							_updateASelector(articulation_x_y, b, nextStep);
							_setupModuleZ(hand, subConfig, nextStep, g);

							wikilibras.updateTempParameterJSON(hand, subConfig,
									step, f + ';' + g);
							dynworkflow.userSelectedAnOption();
						});
	}

	function _setupBallSelectorZ(hand, subConfig, step) {
		var base_id = '.selection-panel-body[mainConfig=' + hand
				+ '][subConfig=' + subConfig + '][step=' + step + ']';
		var articulation_z = base_id + ' .module-z';
		$(articulation_z + ' .ball-selector').off('click').on(
				'click',
				function(a) {
					var b = $(a.target);
					if (!b.hasClass('ball-selector')) {
						dynworkflow.userSelectedAnOption();
						return;
					}
					var c = b.parent('.grid-row'), e = $(articulation_z), h = b
							.attr('data-z');
					b.attr('data-z') && e.attr('data-z', h), _updateASelector(
							articulation_z, b, step);

					wikilibras
							.updateTempParameterJSON(hand, subConfig, step, h);
					dynworkflow.userSelectedAnOption();
				});
	}

	function _calculateArticulationPointIndex(hand, xValue, yValue, zValue) {
		var x = xValue;
		var y = yValue;
		var z = zValue;
		if (hand == 'left-hand') {
			x = MAX_COLUMNS - x + 1;
		}

		var value = (z - 1) * MAX_COLUMNS + x + 3 * MAX_COLUMNS * (y - 1);
		//console.log(value);
		return value;
	}

	articulation.processValue = function(hand, selectionArray) {
		var xyValueSplit = selectionArray[0].split(';');
		var xValue = parseInt(xyValueSplit[0]);
		var yValue = parseInt(xyValueSplit[1]);
		var zValue = parseInt(selectionArray[1]);
		return _calculateArticulationPointIndex(hand, xValue, yValue, zValue);
	};

	articulation.setupModuleXY = function(serverhost, hand, subConfig, step) {
		server_host = serverhost;
		_setupBallSelectorXY(hand, subConfig, step);
	};

	articulation.setupModuleZ = function(serverhost, hand, subConfig, step) {
		server_host = serverhost;
		_setupBallSelectorZ(hand, subConfig, step);

		var selectedY = _getSelectedY(hand, subConfig, step);
		_setupModuleZ(hand, subConfig, step, selectedY);
	};

	articulation.clean = function() {
		$('.ball-selector.active').each(function() {
			$(this).removeClass('active');
			$(this).find('.point-selector').remove();
		});
		$('.module-x-y').attr('data-x', '');
		$('.module-x-y').attr('data-y', '');
		$('.module-z').attr('data-z', '');
	}

}(window.articulation = window.articulation || {}, jQuery));
