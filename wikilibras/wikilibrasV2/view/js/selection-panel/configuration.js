(function(configuration, $, undefined) {

  configuration.setupFingersGroup = function(hand, subConfig, step) {
    var baseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
    		subConfig + '][step=' + step + ']';
    $(baseId + ' .selection-panel-option'
        ).off('click').on('click', function() {
          selectionPanel.selectAnOption(baseId, this);
          _setupFingersToShow(hand, subConfig, step);
          
          dynworkflow.userSelectedAnOption();
        });
  };
  
  function _setupFingersToShow(hand, subConfig, step) {
    var stepOneBaseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
			subConfig + '][step=' + step + ']';
    var nextStep = parseInt(step) + 1;
    var stepTwoBaseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
			subConfig + '][step=' + nextStep + ']';
    
	var finger_group = $(stepOneBaseId + ' .selection-panel-option[select=true]').attr('value');
	finger_group = typeof finger_group == 'undefined' ? '0' : finger_group;
	
	// clean next step
    dynworkflow.cleanStep(hand, subConfig, nextStep);
	$(stepTwoBaseId + ' .finger-group').hide();
	$(stepTwoBaseId + ' .finger-group[group=' + finger_group + ']').show();
  }

  configuration.setupFingersPosition = function(hand, subConfig, step) {
    var stepTwoBaseId = '.selection-panel-body[mainConfig=' + hand + '][subConfig=' +
			subConfig + '][step=' + step + ']';
    $(stepTwoBaseId + ' .selection-panel-option').off('click').on(
        'click', function() {
          selectionPanel.selectAnOption(stepTwoBaseId, this);
          dynworkflow.userSelectedAnOption();
    });
    var previousStep = parseInt(step) - 1;
    _setupFingersToShow(hand, subConfig, previousStep);
  };

}(window.configuration = window.configuration || {}, jQuery));
