(function(wikilibras, $, undefined) {

	var videosUrl = '';
	var baseUrl = '';
	var serverBackendUrl = '';
	var apiUrl = '';
	var uploadSignsUrl = '';
	var currentTaskId = -1;
	var tmpParameterJSON = {};
	var parsedParameterJSON = {};
	var pybossaEndpoint = '';
	var projectName = '';
	var isDemoTask = false;

	function _setupTmpParameterJSON(sign_name) {
		tmpParameterJSON = {
			'sinal' : sign_name,
			'userId' : _getLoggedUser(),
			'facial' : {},
			'right-hand' : {},
			'left-hand' : {}
		};
		parsedParameterJSON = {};
	}

	function _getLoggedUser() {
		var pybossaRememberToken = Cookies.get('remember_token');
		var splittedTokenId = pybossaRememberToken.split('|');
		return splittedTokenId.length > 0 ? splittedTokenId[0]
				: 'anonymous';
	}

	function _loadTaskInfo(task) {
		currentTaskId = task.id;
		var signName = task.info.sign_name;
		var videoRef = task.info.video_ref;
		var videoSource = '';

		if (isDemoTask) {
			var baseRefVidUrl = videosUrl + signName + '_REF';
			videoSource = videoHelper.getSource(baseRefVidUrl);
		} else {
			var webmVideoUrl = uploadSignsUrl + videoRef;
			videoSource = videoHelper.getSourceByWebmUrl(webmVideoUrl);
		}

		$('.sign-label').text(signName);
		$('.ref-video').html(videoSource);
		_setupTmpParameterJSON(task.info.sign_name);
	}

	function _updateTempParameterJSON(mainConfig, subConfig, step, value) {
		var subConfigJSON = tmpParameterJSON[mainConfig][subConfig];
		if (typeof subConfigJSON == 'undefined') {
			tmpParameterJSON[mainConfig][subConfig] = [];
			subConfigJSON = tmpParameterJSON[mainConfig][subConfig];
		}
		subConfigJSON[parseInt(step) - 1] = value;
	}

	function _parseTmpParameterJSON() {
		parsedParameterJSON = tmpJSONParser.parse(tmpParameterJSON,
				selectionPanel.isConfigurationComplete('right-hand'),
				selectionPanel.isConfigurationComplete('left-hand'));
		return parsedParameterJSON;
	}

	function _showInitialScreen(toShow) {
		if (toShow) {
			$("#initial-screen").fadeIn(300);
			videoHelper.play("#initial-screen video");
		} else {
			$("#initial-screen").hide();
			videoHelper.pause("#initial-screen video");
		}
	}

	function _showApprovalScreen(toShow, parameterJSON) {
		if (toShow) {
			$("#render-button-container .btn").hide();
			$("#approval-button").show();
			$("#approval-msg").show();
			renderSign.showRenderedAvatar(parameterJSON);
		} else {
			$("#approval-button").hide();
			$("#approval-msg").hide();
		}
	}

	function _submitAnswer(task, deferred, status) {
		if (!isDemoTask) {
			var answer = _createAnswer(task, status);
			if (status == "APPROVED") {
				_finishTask(task, deferred, answer);
			} else {
				_saveAnswer(task, deferred, answer);
			}
		}
		renderSign.showRenderScreen(false);
		$("#thanks-screen").show();
	}

	function _setupMainScreen(task, deferred) {
		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != "undefined";
		if (hasLastAnswer) {
			_showApprovalScreen(true, lastAnswer.parameter_json);
		} else {
			_showApprovalScreen(false);
			_showInitialScreen(true);
		}
		$("#start-button").off("click").on("click", function() {
			_showInitialScreen(false);
			configurationScreen.show(true);
		});
		$("#ready-button").off("click").on("click", function() {
			if ($(this).hasClass('disabled')) {
				event.preventDefault();
				return;
			}
			renderSign.submit(_parseTmpParameterJSON());
		});
		$("#finish-button").off("click").on("click", function() {
			if ($(this).hasClass('disabled')) {
				event.preventDefault();
				return;
			}
			_submitAnswer(task, deferred, "FINISHED");
		});
		$("#approval-button").off("click").on("click", function() {
			_submitAnswer(task, deferred, "APPROVED");
		});
	}

	function _setupGUI(task, deferred) {
		configurationScreen.setup();
		_setupMainScreen(task, deferred);
	}

	function _createAnswer(task, status) {
		var answer = {}
		answer["status"] = status;
		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != "undefined";

		if (hasLastAnswer && status == "APPROVED") {
			answer["number_of_approval"] = lastAnswer.number_of_approval + 1;
			answer["parameter_json"] = lastAnswer.parameter_json;
		} else {
			answer["number_of_approval"] = 0;
			answer["parameter_json"] = parsedParameterJSON;
		}
		return answer;
	}

	function _finishTask(task, deferred, answer) {
		var lastAnswer = task.info.last_answer;
		var hasLastAnswer = typeof lastAnswer != "undefined";
		var toSubmitUserId = hasLastAnswer ? lastAnswer.parameter_json["userId"]
				: _getLoggedUser();
		$.ajax({
			type : "POST",
			url : serverBackendUrl + "/finish_task",
			data : {
				"task_id" : task.id,
				"project_id" : task.project_id,
				"user_id" : toSubmitUserId,
				"sign_name" : task.info.sign_name,
				"number_of_approval" : answer.number_of_approval
			},
			success : function(response) {
				_saveAnswer(task, deferred, answer);
			},
			error : function(xhr, textStatus, error) {
				alert(xhr.responseText);
			}
		});
	}

	function _saveAnswer(task, deferred, answer) {
		pybossa.saveTask(task.id, answer).done(function() {
			setTimeout(function() {
				$("#thanks-screen").hide();
				deferred.resolve();
			}, 2500);
		});
	}

	function _showCompletedAllTaskMsg() {
		$("#completed-task-msg").hide();
		$("#completed-all-task-msg").show();
		$("#thanks-screen").fadeIn(300);
	}

	function _setupLoginContainer() {
		if ($("#login-container").html() === "") {
			$("#login-container").html(
					$("#main-navbar-collapse .navbar-right li").html());
		}
	}

	function _loadMainComponents() {
		pybossaApiHelper.setup(pybossaEndpoint, projectName);
		loadHtmlHelper.setup(baseUrl);
		iconHelper.setup(baseUrl);
		dynworkflow.setup(baseUrl);

		tutorial.setup(pybossaEndpoint, projectName, _getLoggedUser());
		submitSign.setup(uploadSignsUrl, _getLoggedUser());
		teachedSigns.setup();
		renderSign.setup(apiUrl);
		ranking.setup(baseUrl, pybossaEndpoint, projectName,
				_getLoggedUser());
		_setupLoginContainer();
	}

	function _showDemoTask() {
		isDemoTask = true;
		var task = {'info':{'sign_name':'CALAR'}};
		_startTask(task, function() {});
	}

	function _startTask(task, deferred) {
		_loadTaskInfo(task);
		_setupGUI(task, deferred);
		$("#thanks-screen").hide();
		$("#main-container").fadeIn(500);
	}

	pybossa.presentTask(function(task, deferred) {
		_loadMainComponents();
		if (!$.isEmptyObject(task) && currentTaskId != task.id) {
			_startTask(task, deferred);
		} else {
			_showCompletedAllTaskMsg();
		}
	});

	// Private methods
	function _run(projectname) {
		pybossa.setEndpoint(pybossaEndpoint);
		pybossa.run(projectname);
	}

	// Public methods
	wikilibras.run = function(serverhost, serverbackend, projname, apihost,
			uploadsignshost) {
		baseUrl = serverhost;
		serverBackendUrl = serverbackend;
		videosUrl = baseUrl + "/videos/";
		apiUrl = apihost;
		uploadSignsUrl = uploadsignshost;
		pybossaEndpoint = '/pybossa';
		projectName = projname;
		_run(projectName);
	};

	wikilibras.updateTempParameterJSON = function(mainConfig, subConfig, step,
			value) {
		_updateTempParameterJSON(mainConfig, subConfig, step, value);
	}

	wikilibras.showTeachContainer = function() {
		$(".sub-main-container").hide();
		$("#teach-container").show();
	}

	wikilibras.showTutorialContainer = function() {
		$(".sub-main-container").hide();
		$("#navbar-tutorial-container").show();
	}

	wikilibras.showDemoTask = function() {
		_showDemoTask();
	}

}(window.wikilibras = window.wikilibras || {}, jQuery));