(function(configurationScreen, $, undefined) {
	
	function _isMenuSelected() {
		return $('#configuration-menu .icon_container[select=true]').length > 0;
	}
	
	function _getCurrentMainConfiguration() {
		return _isMenuSelected() ? $(
				'#configuration-menu .icon_container[select=true]').attr(
				'name') : '';
	}
	
	configurationScreen.isMenuSelected = function() {
		return _isMenuSelected();
	}
	
	configurationScreen.getCurrentMainConfiguration = function() {
		return _getCurrentMainConfiguration();
	}
	
	configurationScreen.setup = function() {
		$('.icon_container').off('mouseover').on('mouseover', function() {
			if (iconHelper.canHover(this)) {
				iconHelper.enableIconHover(this, true);
			}
		});
		$('.icon_container').off('mouseout').on('mouseout', function() {
			if (iconHelper.canHover(this)) {
				iconHelper.enableIconHover(this, false);
			}
		});
		$('.config-menu-option').off('click').on('click', function() {
			selectionPanel.show($(this).attr('panel'));
		});
		$('#minimize-icon-container').off('click').on('click', function() {
			$('#ref-video-container').hide();
			$('#minimize-icon-container').hide();
			$('#maximize-icon-container').show();
		});
		$('#maximize-icon-container').off('click').on('click', function() {
			$('#ref-video-container').show();
			$('#maximize-icon-container').hide();
			$('#minimize-icon-container').show();
		});
		selectionPanel.setup();
	};
	
	function _showConfigurationScreen(toShow) {
		if (toShow) {
			$("#configuration-screen").show();
			videoHelper.play("#ref-video-container video");
		} else {
			$("#configuration-screen").hide();
			videoHelper.pause("#ref-video-container video");
		}
	}
	
	configurationScreen.show = function(toShow) {
		_showConfigurationScreen(toShow);
	}

}(window.configurationScreen = window.configurationScreen || {}, jQuery));
