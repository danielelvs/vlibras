(function(renderSign, $, undefined) {

	var apiUrl = '';

	function _submitParameterJSON(parsedParameterJSON, callback) {
		console.log(parsedParameterJSON);
		$.ajax({
			type : 'POST',
			url : apiUrl + '/sign',
			data : JSON.stringify(parsedParameterJSON),
			contentType : 'application/json',
			success : function(response) {
				console.log(response);
				callback(parsedParameterJSON);
			},
			error : function(xhr, textStatus, error) {
				alert(xhr.responseText);
			}
		});
	}

	function _showRenderedAvatar(parameterJSON) {
		var userId = parameterJSON['userId'];
		var signName = parameterJSON['sinal'];

		var avatarBaseUrl = _getRenderedAvatarBaseUrl(userId, signName);
		$('#render-avatar video').html(videoHelper.getSource(avatarBaseUrl));
		$("#render-avatar").fadeIn(300);
	}

	function _showRenderScreen(toShow) {
		if (toShow) {
			$("#render-screen").fadeIn(300);
			videoHelper.play("#render-ref video");
			videoHelper.play("#render-avatar video");
		} else {
			$("#render-screen").hide();
			videoHelper.pause("#render-ref video");
			videoHelper.pause("#render-avatar video");
		}
	}

	function _getRenderedAvatarBaseUrl(userId, signName) {
		return apiUrl + '/public/' + userId + '/' + signName;
	}

	renderSign.showRenderedAvatar = function(parameterJSON) {
		_showRenderedAvatar(parameterJSON);
		_showRenderScreen(true);
	}

	renderSign.showRenderScreen = function(toShow) {
		_showRenderScreen(toShow);
	}

	renderSign.getRenderedAvatarBaseUrl = function(userId, signName) {
		return _getRenderedAvatarBaseUrl(userId, signName);
	}

	renderSign.submit = function(parsedParameterJSON) {
		configurationScreen.show(false);
		_showRenderScreen(true);
		$("#render-avatar").hide();
		$("#render-loading").fadeIn(300);
		$("#render-button-container .btn").hide();
		$("#finish-button").addClass("disabled");
		$("#finish-button").show();

		_submitParameterJSON(parsedParameterJSON,
				function(parsedParameterJSON) {
					$("#render-loading").fadeOut(300);
					$("#finish-button").removeClass("disabled");
					_showRenderedAvatar(parsedParameterJSON);
				});
	};

	renderSign.setup = function(url) {
		apiUrl = url;
		$("#render-edit").off("click").on("click", function() {
			_showRenderScreen(false);
			configurationScreen.show(true);
		});
	}

}(window.renderSign = window.renderSign || {}, jQuery));
