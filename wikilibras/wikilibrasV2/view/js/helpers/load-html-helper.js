(function(loadHtmlHelper, $, undefined) {

	var baseUrl = '';
	var templatesUrl = '';

	_preprocessHtml = function(data) {
		var matchSubConfig = data.match(/sub(?:C|c)onfig="(.*?)"/);
		var currentMainConfig = dynworkflow.getMainConfig(); // right-hand or left-hand
		var goodData = data;

		var isRightHand = function(hand) {
			return hand === 'right-hand';
		};

		var replaceConfigurationTag = function(data, mainConfig) {
			if (isRightHand(mainConfig)) {
				return data.replace(/{{ configuracao }}/g, 'cmd');
			} else {
				return data.replace(/{{ configuracao }}/g, 'cme');
			}
		};

		var replaceOrientationTag = function(data, mainConfig) {
			if (isRightHand(mainConfig)) {
				return data.replace(/{{ orientacao }}/g, 'ord');
			} else {
				return data.replace(/{{ orientacao }}/g, 'ore');
			}
		};

		var replaceHandFolderTag = function(data, mainConfig) {
			if (isRightHand(mainConfig)) {
				return data.replace(/{{ hand-folder }}/g, 'md');
			} else {
				return data.replace(/{{ hand-folder }}/g, 'me');
			}
		};

		var replaceMovementNameTag = function(data, mainConfig) {
			var selectedMovement = movement
					.getPreviousSelectedMovement(mainConfig);
			if (typeof selectedMovement != "undefined") {
				return data.replace(/{{ movement-name }}/g, selectedMovement);
			}
			return data;
		};

		if (matchSubConfig) { // case defined
			// There is no specific(right or left hand dependent) assets for: articulacao, duracao, expressao, movimento, transicao
			// Specific configurations: configuracao, orientacao
			// possible values on the side as comment
			var subConfig = matchSubConfig[1]; // articulacao | configuracao | duracao | expressao | movimento | orientacao | transicao

			// possible subconfigs that need changing
			switch (subConfig) {
			case 'configuracao':
				goodData = replaceConfigurationTag(data, currentMainConfig);
				break;
			case 'configuracao-retilineo':
				goodData = replaceConfigurationTag(data, currentMainConfig);
				break;
			case 'orientacao':
				goodData = replaceOrientationTag(data, currentMainConfig);
				break;
			case 'orientacao-retilineo':
				goodData = replaceOrientationTag(data, currentMainConfig);
				break;
			}
		}
		goodData = replaceHandFolderTag(goodData, currentMainConfig);
		goodData = replaceMovementNameTag(goodData, currentMainConfig);
		goodData = goodData.replace(/{{ hand }}/g, currentMainConfig);
		return goodData.replace(/{{ server }}/g, baseUrl);
	};

	function _getHtml(templatePath, target, toReplace, toPrepend, callback) {
		var url = templatesUrl + templatePath;
		$.get(url, function(data) {
			var processedHtml = _preprocessHtml(data);

			if (toReplace) {
				$(target).html(processedHtml);
			} else if (toPrepend) {
				$(target).prepend(processedHtml);
			} else {
				$(target).append(processedHtml);
			}
		}).done(function() {
			callback && callback(); // call if defined
		});
	}

	loadHtmlHelper.append = function(templatePath, target, toPrepend, callback) {
		_getHtml(templatePath, target, false, toPrepend, callback);
	};

	loadHtmlHelper.load = function(templatePath, target, callback) {
		_getHtml(templatePath, target, true, false, callback);
	};

	loadHtmlHelper.setup = function(url) {
		baseUrl = url;
		templatesUrl = baseUrl + "/templates";
	};

}(window.loadHtmlHelper = window.loadHtmlHelper || {}, jQuery));
