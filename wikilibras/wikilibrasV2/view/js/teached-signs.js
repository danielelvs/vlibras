(function(teachedSigns, $, undefined) {

	var totalTasks = 0;
	var doneTasks = 0;
	var userId = -1;
	var projectId = -1;

	function _updateTeachedSignsMessage() {
		$('.teached-signs-msg').hide();
		if (doneTasks == 0) {
			$('.teached-signs-msg[type=none]').show();
		} else if (doneTasks == 1) {
			$('.teached-signs-msg[type=one]').show();
		} else {
			$('.teached-signs-msg[type=more] span').text(doneTasks);
			$('.teached-signs-msg[type=more]').show();
		}
	}

	function _createSigns(answers) {
		_updateTeachedSignsMessage();
		$('#signs-list-container').html('');
		for (i = 0; i < answers.length; i++) {
			_addSign(answers[i].info);
		}
	}

	function _addSign(answer) {
		var signName = answer.parameter_json.sinal;
		var apiUserId = answer.parameter_json.userId;
		var videoBaseUrl = renderSign.getRenderedAvatarBaseUrl(apiUserId,
				signName);
		$('#signs-list-container').append(
				'<div class="col-btn col-xs-6 col-sm-3 col-md-2" sign-name="'
						+ signName
						+ '"><button class="btn btn-danger btn-block">'
						+ signName + '</button></div>');
		$('#signs-list-container .col-btn[sign-name=' + signName + ']').off(
				'click').on(
				'click',
				function() {
					$('#teached-sign-video-container').html(
							'<video autoplay loop>'
									+ videoHelper.getSource(videoBaseUrl)
									+ '</video>');
					$('#teached-sign-name').html(signName);
					$('#teached-sign-modal').modal('show');
				});
	}

	function _updateTeachedSignsContainer() {
		pybossaApiHelper.getAnswers(projectId, userId).done(function(answers) {
			_createSigns(answers);
		});
	}

	teachedSigns.show = function() {
		$('.sub-main-container').hide();
		$('#teached-signs-container').show();
	}

	teachedSigns.setup = function() {
		pybossaApiHelper.getProjectId().done(function(response) {
			if (typeof response == 'undefined' || response.length < 1) {
				return;
			}
			projectId = response[0].id;
			pybossaApiHelper.getUserProgress().done(function(response) {
				totalTasks = response.total;
				doneTasks = response.done;
				userId = response.user_id;
				_updateTeachedSignsContainer();
			});
		});
	};

}(window.teachedSigns = window.teachedSigns || {}, jQuery));
