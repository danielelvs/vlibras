(function(tutorial, $, undefined) {
	
	var TUTORIAL_DURATION = 30 * 2;
	var pybossaEndpoint = '';
	var projectName = '';
	var loggedUser = '';

	function _finishTutorialSetup() {
		var cookieName = loggedUser + "_" + projectName + '_tutorial';
		var isFirstTime = typeof Cookies.get(cookieName) === 'undefined';
		if (isFirstTime) {
			$("#tutorial-container .modal").modal("show");
		}
		Cookies.set(cookieName, true, {expires : TUTORIAL_DURATION, path: pybossaEndpoint +
			"/project/" + projectName});
	}
	
	tutorial.setup = function(endpoint, name, user) {
		pybossaEndpoint = endpoint;
		projectName = name;
		loggedUser = user;
		loadHtmlHelper.load('/tutorial/tutorial.html', '#tutorial-container', _finishTutorialSetup);
	};

}(window.tutorial = window.tutorial || {}, jQuery));