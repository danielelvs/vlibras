PROJECT_NAME              = wikilibras
VLIBRAS_WIKILIBRAS_ENV   := /etc/profile.d/lavid/vlibras/wikilibras_env.sh
PROJECT_DIR              := $(CURDIR)
SED_REPLACE_ALL           = sed -i "s/$(1)/$(2)/g" "$(3)"
SED_REPLACE_ALL_PATH      = sed -i "s\#$(1)\#$(2)\#g" "$(3)"

-include ${VLIBRAS_WIKILIBRAS_ENV}

ifndef LOCALHOST
    ifndef NETWORK_INTERFACE
        NETWORK_INTERFACE := $(shell ip route | grep "default" | awk '{print $$5; exit}')
    endif
    ifdef NETWORK_INTERFACE
        LOCALHOST ?= $(shell ip route get 1 | awk '{print $$7; exit}')
    endif
endif

ifndef LOCALHOST
    LOCALHOST ?= 127.0.0.1
endif

install: uninstall config env

env:
	@ ( \
	    virtualenv ./env/; \
	    . ./env/bin/activate; \
	    pip install -U pip; \
	    pip install -U uwsgi; \
	    pip install -U ndg-httpsclient; \
	    pip install -r requirements.txt; \
	)

create_project update_project:
	@ echo "http://${LOCALHOST}/${PROJECT_NAME}-backend/$@"
ifdef PYBOSSA_API_KEY
	@ curl "http://${LOCALHOST}/${PROJECT_NAME}-backend/$@" && echo ""
endif

config:
	@ cp settings_local.py.tmpl settings_local.py
	@ $(call SED_REPLACE_ALL_PATH,<path-to-project>,${PROJECT_DIR},settings_local.py)
	@ $(call SED_REPLACE_ALL,localhost,${LOCALHOST},settings_local.py)
ifdef PYBOSSA_API_KEY
	@ $(call SED_REPLACE_ALL,my-api-key,${PYBOSSA_API_KEY},settings_local.py)
endif
	@ cp ./contrib/${PROJECT_NAME}.wsgi.tmpl ./contrib/${PROJECT_NAME}.wsgi
	@ $(call SED_REPLACE_ALL_PATH,<path-to-project>,${PROJECT_DIR},./contrib/${PROJECT_NAME}.wsgi)
	@ cp ./contrib/apache/${PROJECT_NAME}.conf.tmpl ./contrib/apache/${PROJECT_NAME}.conf
	@ $(call SED_REPLACE_ALL,ServerName localhost,ServerName ${LOCALHOST},./contrib/apache/${PROJECT_NAME}.conf)
	@ $(call SED_REPLACE_ALL,user1,$$USER,./contrib/apache/${PROJECT_NAME}.conf)
	@ $(call SED_REPLACE_ALL,group1,$$USER,./contrib/apache/${PROJECT_NAME}.conf)
	@ $(call SED_REPLACE_ALL_PATH,<path-to-project>,${PROJECT_DIR},./contrib/apache/${PROJECT_NAME}.conf)
	@ sudo cp -u "/etc/apache2/sites-available/pybossa.conf" /etc/apache2/sites-available/wikilibras.conf
	@ if grep -q "WSGIScriptAlias /${PROJECT_NAME}" "/etc/apache2/sites-available/wikilibras.conf"; \
	  then \
	      echo "\33[33;1mA previous WSGI daemon definition for '${PROJECT_NAME}' in: /etc/apache2/sites-available/wikilibras.conf\33[0m"; \
	  else \
	      sudo sed -i -e "/Header/,/VirtualHost>/d" /etc/apache2/sites-available/wikilibras.conf; \
	      sudo sed -n -e '/WSGIDaemonProcess/,/VirtualHost>/p' ./contrib/apache/${PROJECT_NAME}.conf | \
	      sudo tee -a /etc/apache2/sites-available/wikilibras.conf > /dev/null; \
	  fi

apache:
	@ sudo a2dissite pybossa.conf
	@ sudo a2ensite wikilibras.conf
	@ sudo apachectl configtest
	@ sudo service apache2 start
	@ sudo service apache2 reload

clean:
	@ find . -regextype posix-awk -regex "(.*.log|.*.pyc)" -type f -print -delete

uninstall:
	@ rm -rf ./env/
	$(MAKE) clean
	@ rm -f ./settings_local.py
	@ rm -f ./contrib/${PROJECT_NAME}.wsgi
	@ sudo rm -f /etc/apache2/sites-available/wikilibras.conf
	@ sudo rm -f /etc/apache2/sites-enabled/wikilibras.conf

run:
	@ ( \
	    . ./env/bin/activate; \
	    python main.py; \
	)
