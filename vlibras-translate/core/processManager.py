#!/usr/bin/env python

import subprocess
from threading import Thread
import signal
import sys
import os

KEEP_RUNNING = True

def signalHandler(signal, frame):
	global KEEP_RUNNING
	KEEP_RUNNING = False
	print("Closing")
	sys.exit(0)

def spawnRenderer():
	while KEEP_RUNNING:
		proc = subprocess.Popen(["/home/vlibras/renderer.py"], shell=True, stdout=subprocess.PIPE)
		print("Process RENDERER PID: " + str(proc.pid))
		try:
			stdoutdata, stderrdata = proc.communicate()
		except:
			print "An error occured, running again..."
	return None

def spawnTranslator():
	while KEEP_RUNNING:
		proc = subprocess.Popen(["/home/vlibras/translator.py"], shell=True, stdout=subprocess.PIPE)
		print("Process TRANSLATOR PID: " + str(proc.pid))
		try:
			stdoutdata, stderrdata = proc.communicate()
		except:
			print "An error occured, running again..."
	return None

def spawnIndexer():
        while KEEP_RUNNING:
                proc = subprocess.Popen(["/home/vlibras/indexer.py"], shell=True, stdout=subprocess.PIPE)
                print("Process INDEXER PID: " + str(proc.pid))
                try:
                        stdoutdata, stderrdata = proc.communicate()
                except:
                        print "An error occured, running again..."
        return None


if __name__ == "__main__":
	signal.signal(signal.SIGINT, signalHandler)

	t = Thread(target=spawnTranslator, args=())
	t.start()

	t2 = Thread(target=spawnRenderer, args=())
	t2.start()

        t3 = Thread(target=spawnIndexer, args=())
        t3.start()
