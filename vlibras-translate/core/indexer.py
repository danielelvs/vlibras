#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Erickson Silva
E-Mail: erickson.silva@lavid.ufpb.br

Author: Wesnydy Lima Ribeiro
E-Mail: wesnydy@lavid.ufpb.br
"""

import os
import Trie
import json
import pika
import PikaManager

from time import sleep

TRIE=None

BUNDLES_PATH=None
BUNDLES_LIST={}

# Manager of queues connections.
manager = PikaManager.PikaManager("rabbit")

def generate_trie():
    global TRIE
    signs = list(BUNDLES_LIST["DEFAULT"])
    TRIE = json.dumps(Trie.gen(signs))

def list_files(path):
    files = []
    for fname in os.listdir(path):
        path_mount = os.path.join(path, fname)
        if not os.path.isdir(path_mount):
            files.append(fname)
    return files

def check_platform_files():
    android = set(list_files(BUNDLES_PATH["ANDROID"]))
    ios = set(list_files(BUNDLES_PATH["IOS"]))
    webgl = set(list_files(BUNDLES_PATH["WEBGL"]))
    standalone = set(list_files(BUNDLES_PATH["STANDALONE"]))
    if android == ios and ios == webgl and webgl == standalone:
        return standalone
    raise RuntimeError("Inconsistent signs. Check files.")

def list_bundles():
    global BUNDLES_LIST
    states = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT",
              "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO",
              "RR", "SC", "SP", "SE", "TO"]
    BUNDLES_LIST["DEFAULT"] = check_platform_files()
    for platform, path in BUNDLES_PATH.iteritems():
        BUNDLES_LIST[platform] = {}
        for state in states:
            try:
                BUNDLES_LIST[platform].update({state:set(os.listdir(os.path.join(path, state)))})
            except OSError:
                BUNDLES_LIST[platform].update({state:set([])})

def load_bundles_paths():
    global BUNDLES_PATH
    try:
        SIGNS_PATH=os.environ['SIGNS_VLIBRAS']
    except KeyError:
        raise EnvironmentError("Environment variable 'SIGNS_VLIBRAS' not found.")
    IOS_SIGNS_PATH=os.path.join(SIGNS_PATH, "IOS")
    ANDROID_SIGNS_PATH=os.path.join(SIGNS_PATH, "ANDROID")
    STANDALONE_SIGNS_PATH=os.path.join(SIGNS_PATH, "STANDALONE")
    WEBGL_SIGNS_PATH=os.path.join(SIGNS_PATH, "WEBGL")
    BUNDLES_PATH={"IOS":IOS_SIGNS_PATH, "ANDROID":ANDROID_SIGNS_PATH,
	              "STANDALONE":STANDALONE_SIGNS_PATH, "WEBGL":WEBGL_SIGNS_PATH}
    list_bundles()
    generate_trie()

def keep_alive(conn_send, conn_receive):
    """
    Keep the connection alive.

    Parameters
    ----------
    conn_send : object
        Connection of writer.
    conn_receive : object
        Connection of receiver.
    """
    while True:
        sleep(30)
        try:
            conn_send.process_data_events()
            conn_receive.process_data_events()
        except:
            continue

# start_new_thread(keep_alive, (manager.get_conn_send(), manager.get_conn_receive()))

def run(ch, method, properties, body):
    """
    Execute the worker.

    Parameters
    ----------
    ch : object
        Channel of communication.
    method : function
        Callback method.
    properties : object
        Message containing a set of 14 properties.
    body : string
        Json string containing the necessary arguments for workers.
    """
    print ("Sending list...")
    manager.send_to_queue("lists", TRIE, properties)
    print ("Ok")

print ("Indexing bundles...")
load_bundles_paths()

print ("Indexer listening...")
while True:
    try:
        manager.receive_from_queue("signals", run)
    except KeyboardInterrupt:
        manager.close_connections()
        os._exit(0)
